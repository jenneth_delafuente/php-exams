# Wideout PHP Exams #
## Name: ##

```
The goal of this exam is to find out the programing skill level and style 
and work ethic of an applicant. 
```

### Instructions ###

Fork this repository to your PERSONAL bitbucket account.
Create a branch with this naming convention firstname.lastname.

Add your full name to readme.md under Wideout Web development PHP Exam

Start programming and commit 3 times per page. That way it will record the status of your work on each commit.

Create the following functionalities for the micro blogging site. You 
may add additional specs if needed to make the application better. 

Commit the sql file under the folder /sql

You have 24 hours to complete this exam. Below are the standard 
specs.


### Database: ###

1. Create a DB schema that has the table name microblog. 

2. Create fields for the following data needed. title, content, date created, thumbnail.


### PHP Backend/Frontend: ###

1. Create a simple blog page that will display blog posts (namely thumbnail, title, content) and display them on descending order.

2. Create a function to submit a blog post and display it on the blog page

3. Create a detailed page to display in full each blog post. And each post should contain one image

4. Create an edit function on the detailed page

5. All pages must be mobile responsive for smartphones and tablets

### Design and Assets: ###

1. All design and assets are not provided. Please use appropriate designs and images for the purpose of the exam

Jenneth Dela Fuente