<?php
/*
$template->assign_vars( array(
    'HOSPITALS'         => $hospitals,
    'SEARCH_ROLES'	    => $select_roles,
    'SEARCH_NAME'	    => $input_name,
    'DO_ACTION'		    => "<input type=\"hidden\" value=\"{$do_action}\" name=\"do_action\"/>",
    'ERROR_UPDATE_MSG'      => $update_err_msg,
    'SYSTEM_MSG'	    => !empty($_GET['system_message']) ? $_GET['system_message'] : $system_msg,	
    'POST_HOSPITAL'	    => "<input value=\"{$hospital_id}\" name=\"hospital_id\" type=\"hidden\"/>",
    'POST_SEARCH_NAME'	    => "<input value=\"{$_GET['search_name']}\" name=\"search_name\" type=\"hidden\"/>",
    'POST_SEARCH_USERNAME'   => "<input value=\"{$_GET['search_username']}\" name=\"search_username\" type=\"hidden\">", 
    'POST_PAGEID'	    => "<input value=\"{$_GET['pageID']}\" name=\"pageID\" type=\"hidden\">",
    'DOWNLOAD'		    => $download_link,
	'delete_state'		=> $delete_state,
    'PAGING'		    => $links['all']
    
));
 
*/


global $HTML;
$head = $HTML['head'];
$head = !is_array($head) ? array($head) : $head;
$template->assign_vars( array('HEAD' => implode("\n\t",$head)));

// sample use : add css file to the template
// add_css_file($root_path.'skin/css/test.css');
// sample use : add js file to the template
// add_js_file($root_path.'js/test.js');
// sample use : add head entries to the template (array or string parameter)
// add_head('');

$tpl_files =& get_template_files();

if(!empty($tpl_files['header'])) {
	$template->pparse('header');  
}
?>