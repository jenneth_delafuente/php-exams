<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<?php $tpl_files =& get_template_files(); 
		unset($tpl_files['sidebar']);
		unset($tpl_files['sidebar_left']);
		unset($tpl_files['sidebar_right']);
		
		// set the final template files
		$template->set_filenames($template_files);
		
	?>
	<?php // head
		
		if(!@$HTML['no_head']) {
			require_once 'page/header.php';
		}	
	?>
	<body>
		<div id="container">
				<?php // top
					
					if(!@$HTML['no_top']) {
						require_once 'page/top.php';
					}	
				?>
				<?php // menu
					
					if(!@$HTML['no_menu']) {
						require_once 'page/menu.php';
					}	
				?>
				<?php // sidebar
					
					if(!@$HTML['no_sidebar']) {
						require_once 'page/sidebar-default.php';
					
						if(isset($tpl_files['sidebar_left'])) {
							require_once 'page/sidebar-left.php';	
						}
						if(isset($tpl_files['sidebar_right'])) {
							require_once 'page/sidebar-right.php';	
						}
						
					}	
				?>	
				<?php 
					// dynamic content
					$template->pparse('content');
				?>
				<?php // footer
					
					if(!@$HTML['no_footer']) {
						require_once 'page/footer.php';
					}	
				?>
		</div><!--  /#container -->			
		<!-- this is default.php -->
	</body>
</html>