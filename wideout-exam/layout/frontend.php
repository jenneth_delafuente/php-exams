<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<?php $tpl_files =& get_template_files(); 
		$tpl_files['header'] = 'header_bootstrap_frontend.html';
		$tpl_files['footer'] = 'footer_bootstrap_frontend.html';
		unset($tpl_files['sidebar']);
		unset($tpl_files['sidebar_left']);
		unset($tpl_files['sidebar_right']);
		
		// set the final template files
		$template->set_filenames($template_files);
		
	?>	
	<?php // head
		
		if(!@$HTML['no_head']) {
			require_once 'page/header.php';
		}	
	?>
	 
	<body>
	    <div class="blog-masthead">
	      <div class="container">
	        <nav class="blog-nav">
	          <a class="blog-nav-item active" href="#">WideOut Blog</a>	          
	        </nav>
	      </div>
	    </div>
	    
	<?php 
				// dynamic content
				$template->pparse('content');
	?>
	    
	    
   	    
	</body>	
</html>

