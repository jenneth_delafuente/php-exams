<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<?php $tpl_files =& get_template_files(); 
		$tpl_files['header'] = 'header_bootstrap.html';
		$tpl_files['footer'] = 'footer_bootstrap.html';
		unset($tpl_files['sidebar']);
		unset($tpl_files['sidebar_left']);
		unset($tpl_files['sidebar_right']);
		
		// set the final template files
		$template->set_filenames($template_files);
		
	?>	
	<?php // head
		
		if(!@$HTML['no_head']) {
			require_once 'page/header.php';
		}	
	?>
	 
	<body>
		<!-- Fixed navbar -->
	    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="#">Wide-Out Exams Backend</a>
	        </div>	       
	      </div>
	    </nav>
		
		<div class="container-fluid">
			<?php 
				// dynamic content
				$template->pparse('content');
			?>
			<?php // footer
				
				if(!@$HTML['no_footer']) {
					require_once 'page/footer.php';
				}	
			?>		
		</div><!-- .container-fluid -->
	</body>	
</html>

