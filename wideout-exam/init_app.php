<?php
/**************************
* Initial Calls (Bootstrap)
**************************/
require_once LIB_DIR . 'Mysqli/DB_Mysqli.php';
require_once LIB_DIR . 'Mysqli/List_Form.php';
require_once LIB_DIR . 'Mysqli/List_Iterator_Mysqli.php';
require_once LIB_DIR . 'Mysqli/Table_Mysqli.php';
require_once LIB_DIR . 'Mysqli/Table_Form.php';
require_once LIB_DIR . 'Form_Fields.php';

// DB DSN
// mysqli
$db = new DB_Mysqli($dbhost, $dbuser, $dbpasswd, $dbname);
$db->connect();

// Template  Setup
// templates sections that can be changed by modules/<file>.php script
$template_files =
		array(
				'header' 		=> 'header.html',
				'top'	 		=> 'top.html',
				'menu'	 		=> 'menu.html',
				'sidebar' 		=> 'sidebar-default.html',
				'sidebar_left' 	=> 'sidebar-left.html',
				'sidebar_right' => 'sidebar-right.html',
				'content' 		=> "{$content_template}.html",
				'footer'  		=> 'footer.html'	
			);
?>