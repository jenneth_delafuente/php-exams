<?php
class ListIterator_FrontEnd extends List_Iterator_Mysqli {
	function getRowsHtml() {

        foreach($this->enum as $k=>$v) {
            $row_class = ($k % 2 ==0) ? 'even' : 'odd';
            
            $tr = "<tr class=\"{$row_class}\">\n";
            if(isset($v['row_number'])) {
		        $tr .= <<<EOT
					        <td class="row_number_data">
					            {$v['row_number']}
					        </td>    
EOT;
            }
            
            if(isset($v['row_selector'])) {
		        $tr .= <<<EOT
					        <td class="row_number_data">
					            {$v['row_selector']}
					        </td>    
EOT;
            }
                        
            foreach($this->columns  as $name=>$field) {
                
                 $col_value =   $v[($name)] ;
 
                $width = $this->get_width($name);
                $tr .= "
                    <td class=\"{$name}_data\" width=\"{$width}\">
                        {$col_value}
                    </td>
                ";
                 
            }
            
            $tr .= "
                </tr>
            ";
            
            $html .= $tr;
        }
        
        $html ='';
       foreach($this->enum as $k=>$v) {
        	$date_created = date('F d, Y ');
        	if(empty($v['featured_image'])) :
        		$img = '';
        	else :
				$img = '<div style="width:100%;border:solid 1px #444444;overflow:hidden;"><img src="'.FEATURED_IMG_DIR.$v['featured_image'].'"/></div>';
        	endif;
        	
        	
        	
			$v['post_content'] = substr($v['post_content'], 0, 300);
        	$html .= <<<EOS
        		<div class="blog-post">
            		<h2 class="blog-post-title">{$v['post_title']}</h2>
            		<p class="blog-post-meta">{$date_created} by <a href="#">{$v['author']}</a></p>
            		
            		<div class="row">
            			<div class="col-md-12 col-lg-12 ">{$img}</div>
            			<div class="col-md-12 col-lg-12">{$v['post_content']}</div>
            		</div>
            	</div>	
EOS;
        }
        
        if(count($this->enum) == 0) {
        	$cnt = count($this->columns);        	
			$html = <<<EOS
        		<div class="blog-post">
            		<h2 class="blog-post-title"></h2>
            		<p class="blog-post-meta">No articles found.</p>
            	</div>	
EOS;
        }
        
        return $html;         

    }	
}

class ListForm_FrontEnd extends List_Form {
	
	function __construct($db,$iterator,$fields=array(), $attr=array(), $rows_per_page=10, $links_per_page=5,$action='',$method='post',$params='', $is_ajax=false, $selector='.list' ) {
		parent::__construct($db,$iterator,$fields, $attr, $rows_per_page, $links_per_page,$action,$method,$params, $is_ajax, $selector='.list' );
	} 	
	
	
	function getListHtml()  {
	    // call pagination
		$this->paginate();
		$paged_links = $this->paginationHtml();
		
		// is form submitted in ajax mode
		if($this->is_ajax_submit()) {
			$r['pages'] = $paged_links;
			$r['list'] =  $this->listHtml();
			return $r;
		}

        
					
			$listhtml = 
					'<div class="col-sm-10 blog-main">'
					. $paged_links
					. $this->listHtml()
					. $paged_links
					.'</div>';
			
		 	
		return $listhtml . $this->getJsCode();
            
    }

    function listHtml() {
        parent::listHtml();      	
    	return $this->iterator->getRowsHtml() ;
    }
}



?>