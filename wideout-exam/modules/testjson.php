<?php
/*
 * module sample without any layout or template ideal for json 
 * and other output that can be processed as xml or javascript objects
 * to access : index.php?module=testjson&layout=no_layout
 */
echo json_encode(array('name' => 'jenn'));
?>