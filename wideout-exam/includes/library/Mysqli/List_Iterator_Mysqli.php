<?php
/*********************************************************************************************************
 *
 * sets up columns for listing
 * query execution and query result generation
 * based on query
 * TO DO : clickable links for sorting columns at column header, toggling ascending or descending
 *********************************************************************************************************/
/**
 * References
 * 
 * Simplest column constructor :
 * 
 * $cols['name']['title']
 * $cols['name']['field_source'] (any valid mysql expression)
 * $cols['name']['width']
 * $cols['name']['align'] = L/C/R/J
 *
 * or $cols['name'] = array('title' => '', 'field_source' => '', 'width' => '', 'align' => '');
 * 
 * where 'name' is actual table field or a field name whose values come from any expression or computation (run time fields)
 * 
 * Column Attributes :
 * title, field_source, width, align
 * 
 * Column Helpers
 *      add_column(name, column_attribute=array())
 *      remove_column(name);
 *      set_attrib(name, column_attribute=array()) 
 *      set_title(name, title)
 *      set_width(name, width)
 *      set_align(name, alignment)
 *      set_source(name, any_valid_mysqli_expression)
 *      get_width(name)
 *      get_title(name)
 *      get_align(name)
 *      get_source(name)
 *
 *      // runtime extra field based on valid mysql field expression (option_id and option_name)
        $iterator->add_column('extra_column',array('field_source' => 'CONCAT_WS(" ", option_id, option_name)', 'title' => 'Runtime Extra Field')
 *                      
 *                      );      
 *
 * Table Source Helpers
 *      set_join_tables(join_tables=array() | tablename);
 *      add_join_tables(tablename)
 *      set_join_fields(join_fields=array())
 *      add_join_fields(field join expression)
 *
 *
 * Listing Selection
 *
 *  add_filter(where_condition_expression)
 *  set_filter(array_condition=array())
 *
 *  Sorting
 *      add_sort(sort_expr=array(name => array('name' => direction))
 *      set_sort(array(name => direction))
 *
 *      where direction is ASC or DESC
 *
 *  Adding Links to column
 *      add_link_column(column name)
 *      set_link_column(@array column_names)
 *
 *  Row Modifiers
 *  $iterator->onEnum = 'onRow'
 *
 *  function synopsis : 
 *
 *  function onRow(&$row) {
 *      $row->column_name = < modify the display of this column >
 *  }
 *
 *  
 *  Link Properties
 *      $iterator->link_url = url
 *  Rows per Page
 *      $iterator->rows_per_page = default is 10
 *
 *  Render Iterator
 *
 *      getHtml()
 *      
*/
 
require_once  LIB_DIR . 'Mysqli/Table_Mysqli.php';

class List_Iterator_Mysqli {
    var $db,
        $table,
        $rows            = array(),
        $columns         = array(),
        $join_tables     = array(),
        $join_on         = array(),
        $filter          = array(),
        $sort_columns    = array(),
        $link_columns    = array(),
        $enum            = array(),
        $columns_sql,
        $join_sql,
        $where_sql,
        $order_sql,
        $link_url,
        $rowset,
        $row_count,
        $start,
        $rows_per_page,
        $onEnum,
        $onStyle,
        $onHeader,
        $onFooter;
    
    /**
     *
     *
     **/
    function __construct($db,$table,$columns=array(),$filter=array(), $sort_columns=array(), $link_columns=array(), $link_url='', $join_tables=array(),$join_on=array(), $start=0, $rows_per_page=10 ) {
        $this->db           = $db;
        $this->table        = new Table_Mysqli($table,$db);
        $this->columns      = $columns;
        $this->join_tables  = is_array($join_tables) ? $join_tables : array($join_tables);
        $this->join_on      = is_array($join_on) ? $join_on : array($join_on);
        $this->filter       = is_array($filter) ? $filter : array($filter);
        $this->sort_columns = array($sort_columns);
        $this->link_columns = is_array($link_columns) ? $link_columns : array($link_columns);
        $this->link_url     = $link_url;
        $this->start        = $start;
        $this->rows_per_page = $rows_per_page;
        $this->def_attrib = array(    'title' => '',
                                'width' => 100,
                                'align' => 'L',
                                'field_source' => ''
                            );        
    }
    
    /**
     * @name = column name
     * @attr( 'title' => '', 'source' => '', 'width' => '', 'align' => 'L/C/R/J', 'field_source' (any valid mysql field alias))
     **/
    function add_column($name,$attr=array()) {
        
        $this->merge_attrib($attr,$this->def_attrib);
        $this->columns[$name] = $attr;    
    }
    
    /**
     *
     **/
    function remove_column($name) {
        unset($this->columns[$name]);
    }
    
    /**
     * @attr( 'title' => '',   'width' => '', 'align' => 'L,R,C,J (left/center/right/justified)')
     **/
    function set_attrib($name,$attr=array()) {
        $v = $this->merge_attrib($v,$this->def_attrib);  
        $this->columns[$name]= $v;
    }
    
    /**
     *
     */
    function set_title($name,$title) {
        $this->columns[$name]['title'] = $title;
    }
    
    /**
     *
     */
    function set_width($name,$width) {
        $this->columns[$name]['width'] = $width;
    }
    
    /**
     *
     */
    function set_align($name,$align) {
        $this->columns[$name]['align'] = $align;
    }     
    
 
    
    /**
     *
     */
    function set_source($name,$alias) {
        $this->columns[$name]['field_source'] = $alias;
    }     
    
    /**
     *
     */
    function get_width($name) {
        return (empty($this->columns[$name]['width']) ? $this->def_attrib['width'] : $this->columns[$name]['width']);
    }

    /**
     *
     */
    function get_title($name) {
        return (empty($this->columns[$name]['title']) ? ucwords(str_replace( '_' ,' ', $name )) : $this->columns[$name]['title']);
    }
   
    
    /**
     *
     */
    function get_align($name) {
        return (empty($this->columns[$name]['align']) ? $this->def_attrib['align'] : $this->columns[$name]['align']) ;
    }
    /**
     *
     */
    function get_source($name) {
        return $this->columns[$name]['field_source'];
    }    
    
     /**
      *
      * @array merge_attrib(@attr, $def_attrib)
      **/
     function merge_attrib($attr=array(), $def_attrib=array()) {
      $ret = array();
      foreach($def_attrib as $k=>$v) {
        if(!isset($attr[$k])){
          $ret[$k] = $v;
        } else {
          $ret[$k] = $attr[$k];
        }
      }
      
      foreach($attr as $k=>$v) {
        if(array_key_exists($k, $def_attrib)=== FALSE) {
          $ret[$k] = $v;
        }
      }
      return $ret;
     }
     
    /**
     *
     **/
    function make_array($data) {
        
        if(!empty($data)) {
            if(!is_array($data)) {
                $t = explode(',',$data);
                
                if(count($t)>0) {
                    $data = $t;
                } else {
                    $data = array($data);
                }
                
            }
            
            return  $data;
        } else {
            return array();
        }
    }
    
    /**
     *
     **/
    function set_join_tables($join_tables=array()) {
        foreach($this->make_array($join_tables) as $k=>$v) {
            $this->add_join_tables($v);
        }
    }
    
    /**
     * table expr may include alias. or any valid mysql table expression
     **/
    function add_join_tables($table_expr) {
        $this->join_tables[] = $table_expr;
    }
 
    /**
     *   any valid mysql field expression
     **/
    function add_join_fields($field_expr) {
        $this->join_on[] = $field_expr;
    }    
    
    /**
     *
     **/
    function set_join_fields($join_fields=array()) {
        foreach($this->make_array($join_fields) as $k=>$v) {
            $this->join_on[] = $v;
        }
    }
        
    

    /**
     *   any valid mysql expression
     **/
    function add_filter($filter_expr) {
        $this->filter[] = $filter_expr;
    }    
    
    /**
     *
     **/
    function set_filter($filter=array()) {
        foreach($this->make_array($filter) as $k=>$v) {
            $this->filter[]=$v;
        }
    }
    
    /**
     *   any valid mysql expression
     *   @sort_column[expr] = '', @sort_column['order'] = asc/desc
     **/
    function add_sort($sort_expr=array()) {
        $this->sort_columns[] = $sort_expr;
    }    
    
    /**
     *
     **/
    function set_sort($sort_col=array()) {
        foreach( $sort_col  as $k=>$v) {
            $this->sort_columns[]= array_combine(array('expr','order'),array($v['expr'], $v['order']));
        }
    }
    
    /**
     *   
     **/
    function add_link_column($name) {
        $this->link_columns[] = $name;
    }    
    
    /**
     *
     **/
    function set_link_column($name=array()) {
        foreach($this->make_array($name) as $k=>$v) {
            $this->link_columns[]=$v;
        }
    }
    
    /**
     * sql parser
     **/
    function parseSQL() {
        foreach($this->columns as $name=>$field) {
            if(!empty($this->columns[$name]['field_source'])) {
                $columns .= "{$field['field_source']} AS {$name},"; 
            } else {
                $columns .= "{$name},";
            }
        }
        $columns = substr($columns,0,strlen($columns)-1);
        $this->columns_sql = $columns;
         
        // join
        if(strlen(implode('',$this->join_tables)) > 0) {          
            $t_join_tbl = $this->join_tables;
            $t_join_on = $this->join_on;
            $tbl1   = array_shift($t_join_tbl);
            $expr1  = array_shift($t_join_on);            
            $join  = "LEFT JOIN {$tbl1} ON {$expr1} ";            
            foreach($t_join_tbl as $k=>$v) {
                $join_expr = $t_join_on[$k];
                $join .= " LEFT JOIN {$v} ON {$join_expr} ";
            }
            
        }
         
        $this->join_sql = $join;
        
        // where
        $tmp_filter = strip_out_empty_elements($this->filter);
        $this->filter = $tmp_filter;
        if(strlen(implode('',$this->filter )) > 0) {  
            $where = implode (' AND ', $this->filter);
        }
        else {
            $where = '"1"';
        }
        
        $this->where_sql = $where;
 
        // order        
        if(strlen(implode('', $this->sort_columns) ) > 0) {
            foreach($this->sort_columns as $k=>$v) {
            	if(!empty($v['expr'])) {
                    $sort_c .= "{$v['expr']} {$v['order']}, ";
                }                
            }
 
            $sort_c = substr($sort_c,0,strlen($sort_c)-2);
            $sort_c = empty($sort_c) ? '"1"' : $sort_c;
           
            
        } else {
            $sort_c = '"1"';
        }
        
        
        
        $this->order_sql = $sort_c;
    }
 
    
    /**
     * sql builder
     **/
    function getMySQL() {
        // call the parser
        $this->parseSQL();
        //columns
        $columns = $this->columns_sql;        
        // join
        $join = $this->join_sql; 
        // where
        $where = $this->where_sql;
 
        // order
        $sort_c = $this->order_sql;
 
        $select_total = "SELECT COUNT(*) as total FROM {$this->table->_tablename}
                        {$join}
                        WHERE {$where}
                        ORDER BY {$sort_c};";
                        
        $this->db->exec($select_total);
        if($this->db->query_result) {
        	$t_row = $this->db->query_result->fetch_object();
        }
        $this->info['total_record'] =  $t_row->total;
       
        $this->start = empty($this->start) ? 0 : $this->start ;
        $select = "SELECT {$this->table->_tablename}.*, {$columns} FROM {$this->table->_tablename}
                        {$join}
                        WHERE {$where}
                        ORDER BY {$sort_c}
                        
                        LIMIT {$this->start}, {$this->rows_per_page}
                          ";
                        
        //echo $select;
        return $select;
        
    }
    
    /**
     *
     **/
    function getRows() {
        $query = $this->getMySQL();
        $result = $this->db->exec($query);
        $this->rowset = $this->db->query_result;
       
        if(!$result) {
            print_r($this->db->last_error);
            $this->debug($query);
            die();
        }
        return $result;
    }
    
   
    /**
     *
     **/
    function getEnum() {
        $this->getRows();
        
        $row_num =  $this->start +1;
        
         while($row = $this->rowset->fetch_object()) {
            $fields = get_object_vars($row);
            $fields['row_number'] = $row_num;
            $this->enum[] = $fields;
            ++$row_num; 
         }
         /*link_column formatting , default value  */
         foreach($this->enum as $k=>$v) {
            //$this->onRow($v);
            $this->enum[$k] = $this->onRow($v);
         }
    }
    
    /**
     * allow override for any desired row processing or formatting
     **/
    /* allow user to make changes to the list item (add , remove or format columns) before rendering for added flexibility */
    function onRow(&$row) {
        foreach($this->columns  as $name=>$field) {          
            $col_value =   $row[($name)] ;

            $str_t = implode(',',$this->link_columns).',';
            if(strpos($str_t,$name) !== False) {
                $pk= $row[($this->table->_key)];
                $col_value = "<a href=\"{$this->link_url}?{$this->table->_key}={$pk}\">{$col_value}</a>"; 
            }
            $row[$name] = $col_value;
        }
        
        $cb = $this->onEnum;
        if(!empty($cb)) // user callback is present
        {
            if(!function_exists($cb)) {
                die($cb. ' function is not defined');
            }
            $cb($row,$this);
        }
        
        return $row;
        
    }    
    
    /**
     *
     **/
    function getHtml() {
        $this->getEnum();
        $style = $this->styleList();
        return
                "
                {$style}
				<!-- div id=\"listing-grid\" -->
					<table class=\"list\"   cellspacing=\"0\" cellpadding=\"1\">
					" .
					$this->getHeader() .
					$this->getRowsHtml() .
					$this->getFooter() .
					"
					</table>
				<!-- /div -->
				";
                
    }
    /**
     *
     **/
    function styleList() {
    
        if($this->onStyle) { /* callback to generate css if set from outside such as list form */
            $cb = $this->onStyle;
            return $cb($this);
        }
        
        // column formatting
        foreach($this->columns as $name=>$field) {
            
            $align = $this->get_align($name);
            switch($align) {
                case 'J': $align='justify';break;
                case 'R': $align='right';break;
                case 'C': $align='center';break;
                default:  $align = 'left';break;
            }
            $width = $this->get_width($name);
            
            $td_style .= "\n\t\t\t\t  td.{$name}_data {
                width:{$width}px;
                text-align:{$align};
            }";    
        }
        return 
        
                "<style>
                    table.list {width:100%;border-collapse:collapse;}
                    table.list td, table.list th { border:solid 1px;}
                    {$td_style}
                    th.row_num_header {text-align:center;}
                    td.row_number_data {width:25px;text-align:center;} 
                    tr.odd { background:none #fff;}
                    tr.even { background:none #f4f4f4;}
                    tr.footer td { height:35px;font-weight:bold;vertical-align:middle;}
                </style>";
    }            
    
    /**
     *
     **/
    function getHeader() {
        if($this->onHeader) { /* callback to generate list title was set from outside such as list form */
            $cb = $this->onHeader;
            return $cb($this);
        }

        $header = "<tr>\n";
        
        if(isset($this->enum[0]['row_number'])) {
	        $header .= "
	            <th class=\"row_num_header\">#</th>
	        ";
        }
        
    	if(isset($this->enum[0]['row_selector'])) {
    		$checkbox = "<input type=\"checkbox\" value=\"\" class=\"toggle_selectors\"  id=\"toggle_selectors\" onclick=\"javascript:toggleSelectors(this);\" name=\"toggle_selectors\"/>";
	        $header .= "
	            <th class=\"row_num_header\">{$checkbox}</th>
	        ";
        }        

        
        $php_self = htmlspecialchars($_SERVER['PHP_SELF'] );
        
        $req = $_REQUEST;
        
        unset($req['sort']);
        unset($req['order']);
        unset($req['page']);
        
        $uri = $this->php_self . '?page=1' ;
        
        foreach($req as $k=>$v) {
        	$uri .= "&{$k}={$v}";	
        }
        
        foreach($this->columns as $name=>$field) {
       	
        	$sortfield =  !empty($this->columns[$name]['field_source']) ? 
        				$this->columns[$name]['field_source'] : $name;
        	
        	if(empty($_REQUEST['sort'])) {
        		$sortorder=  
        		            (
        		            $name == $this->sort_columns[0]['expr'] ||
        					$this->columns[$name]['field_source'] == $this->sort_columns[0]['expr']
        					) && $this->sort_columns[0]['order'] == 'asc' 
        					? 'desc'
        					: 'asc';
        			
        	} else {;
				$sortorder=  
        		            (
        		            $name == $_REQUEST['sort'] ||
        					$this->columns[$name]['field_source'] == $_REQUEST['sort']
        					) && $_REQUEST['order'] == 'asc' 
        					? 'desc'
        					: 'asc';        	
        	} 
        	
        	$link_uri = $uri."&sort={$sortfield}&order={$sortorder}";
            
            
        	$col_value = "<a class=\"sort-link\" href=\"{$link_uri}\" sort='{$name}' order='{$sortorder}'>". $this->get_title($name) . $this->toggleSort($name,$sortorder ) . "</a>"; 
            $header .= "<th class=\"{$name}_data\">{$col_value}</th>";
           
        }
        
        $header .= "
                </tr>";
        
        return $header;
    }
    
    /**
     *
     **/
    function toggleSort($name,$sortorder) {
        return    ' <img src="'.base_dir_uri().'www/images/'.($sortorder == 'desc' ? 'arrow-down.jpg' : 'arrow-up.jpg') . '"/>';
    }
    
    /**
     * 
     **/
    function getRowsHtml() {

        foreach($this->enum as $k=>$v) {
            $row_class = ($k % 2 ==0) ? 'even' : 'odd';
            
            $tr = "<tr class=\"{$row_class}\">\n";
            if(isset($v['row_number'])) {
		        $tr .= <<<EOT
					        <td class="row_number_data">
					            {$v['row_number']}
					        </td>    
EOT;
            }
            
            if(isset($v['row_selector'])) {
		        $tr .= <<<EOT
					        <td class="row_number_data">
					            {$v['row_selector']}
					        </td>    
EOT;
            }
                        
            foreach($this->columns  as $name=>$field) {
                
                 $col_value =   $v[($name)] ;
 
                $width = $this->get_width($name);
                $tr .= "
                    <td class=\"{$name}_data\" width=\"{$width}\">
                        {$col_value}
                    </td>
                ";
                 
            }
            
            $tr .= "
                </tr>
            ";
            
            $html .= $tr;
        }
        
        if(count($this->enum) == 0) {
        	$cnt = count($this->columns);        	
        	$tr = "<tr class=\"no-records-found\"><td colspan=\"100\">No records found.</td></tr>";
        	$html = $tr;
        }
        
        return $html;         

    }
    
    /**
     *
     **/
    function getFooter() {
        if($this->onFooter) { /* callback to generate list footer was set from outside such as list form */
            $cb = $this->onFooter;
            return $cb($this);            
        }
        
        if(count($this->enum) > 0) {
	        $start = $this->start + 1;
	        $ending =  (($this->start /  $this->rows_per_page) + 1 ) * $this->rows_per_page;
	        
	        $current_page = ($this->start / $this->rows_per_page ) + 1;
	        $total_page = ceil($this->info['total_record'] / $this->rows_per_page );
	        reset($this->enum);
	        $first_row = current($this->enum);	        
	        
	        /**
	        * return "<tr class=\"footer\"><td colspan=\"". count($first_row) . "\">Total : {$start} - {$ending} / {$this->info['total_record']} Page : {$current_page} of {$total_page} </td></tr>";
			**/
	        
	        // modified -->>	        
		        if($current_page < $total_page) {
		        	if(count($this->enum) < $this->rows_per_page) {
		        		$ending_batch = count($this->enum);
		        	} else{
		        		$ending_batch = $ending;
		        	} 	        			        	
		        } else {
		        	if(count($this->enum) < $this->rows_per_page) {
		        		$ending_batch = $start + (count($this->enum) - 1);
		        	} else{
		        		$ending_batch = $ending;
		        	} 
		        }
		        
		        return "<tr class=\"footer\"><td colspan=\"". count($first_row) . "\">Total : {$start} - {$ending_batch} / {$this->info['total_record']} Page : {$current_page} of {$total_page} </td></tr>";	        
	        // end ---->>
        }
    }
    
    function debug($testvar,$text='',$printvar=1) {
       if($printvar) {
           echo '<pre>',print_r($testvar, true), '</pre>';
   
    
       }
       
       if($testvar) {
            echo '<pre>',print_r($text, true), '</pre>';
       }
    }       
    
    
}

if(!function_exists('strip_out_empty_elements')) {
	
	function strip_out_empty_elements($arr=array()) {
		 $ret= array();
		 foreach($arr as $index=>$value) {
		 	if(!empty($value)) {
		 		$ret[] = $value;
		 	}
		 }
		 return $ret;
	}
}
?>