<?php
/*********************************************************************************************************
 * ListForm by Jei Dela Fuente
 * September 24, 2012
 *
 *
 * - takes care of get parameter handling for pagination, sorting, and query of search values
 * - provides pagination links
 * - other fancy appearance of list form or grid
 * - TODO a child extension of this supporting ajax searching, editing, sorting and navigation
 * - TODO dropdown list to provide choice of number of rows per page in the list
 * UPDATE 
 *  added sort_columns as hidden field forms
 * 
 *********************************************************************************************************/

require_once LIB_DIR . 'Mysqli/List_Iterator_Mysqli.php';
require_once LIB_DIR . 'Form.php';
class List_Form extends  Form {
    var $iterator,
        $params,
        $sort_params,
        $db,
        $total_rows,
        $max_pages,
        $rows_per_page,
        $links_per_page,
        $page,
		$is_ajax,
		$selector,
		$form_method,
        $php_self,
        $add_url,
        $edit_url,
        $delete_url,
        $onRow;

    function __construct($db,&$iterator,$fields=array(), $attr=array(), $rows_per_page=10, $links_per_page=5,$action='',$method='post',$params='', $is_ajax=false, $selector='.list' ) {
    	
		$fields['rows_per_page']['type']	= 'hidden';
		$fields['rows_per_page']['value']	= !empty($_REQUEST['rows_per_page'])  ? $_REQUEST['rows_per_page'] :  $rows_per_page;
		
		$fields['page']['type']	= 'hidden';
		$fields['page']['value']= intval($_REQUEST['page'] );		
		
    	 // added sorting
    	 if(!empty($iterator->sort_columns)) {
    	 		$fields['sort']['type']		= 'hidden';
    	 		$fields['sort']['value']	= !empty($_REQUEST['sort'])  ? $_REQUEST['sort'] :  $iterator->sort_columns[0]['expr'];
    	 		
    	 		$fields['order']['type']	= 'hidden';
    	 		$fields['order']['value']	= !empty($_REQUEST['order'])  ? $_REQUEST['order'] :  $iterator->sort_columns[0]['order'];
				
    	 		$this->sort_params ="&sort={$fields['sort']['value']}&order={$fields['order']['value']}";

    	 }
		 
		 $this->sort_params .= "&rows_per_page={$fields['rows_per_page']['value']}";
		 $fields['selected_ids']['type'] = 'hidden';
		 $fields['action_delete']['type'] = 'hidden';
		 $fields['action_delete']['value'] = 0;
		 
         parent::__construct($fields,$attr,$action,$method);
        
         $this->iterator                =   $iterator;
         $this->params                  =   $params;
         $this->db                      =   $db;
         $this->links_per_page          =   $links_per_page;
         $this->rows_per_page           =   $rows_per_page;
		 $this->is_ajax					= 	$is_ajax;
		 $this->selector				= 	$selector;
		 $this->form_method				=   $method;
		 $this->setFormName('listform');
         $this->php_self = htmlspecialchars($_SERVER['PHP_SELF'] );
         if (isset($_REQUEST['page'] )) {
                $this->page = intval($_REQUEST['page'] );
         }

         $this->iterator->onEnum = 'addSelector';     
         
         $this->listButtons['add'] = array( 'value' => 'Add');
         $this->listButtons['delete'] = array('onclick' => "javascript: return {$this->formName}_delete();" ,'value' => 'Delete');
    }
    
    
    /**
     *
     **/
    function getListHtml()  {
	    // call pagination
		$this->paginate();
		$paged_links = $this->paginationHtml();
		
		// is form submitted in ajax mode
		if($this->is_ajax_submit()) {
			$r['pages'] = $paged_links;
			$r['list'] =  $this->listHtml();
			return $r;
		}

        
        $listhtml =	
            '<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				<td>'.
					$paged_links .  
				'</td>
				<td align="right">'. $this->selectLimiter() .
				'</td>
				</tr>
				<tr><td colspan="2">' .
				'<div id="listing-grid">' .						
					$this->listHtml()  .
				'</div>' .
				'</td>
				</tr>'.
				'<tr>
				<td>'.
					$paged_links .  
				'</td>
				<td align="right">'. $this->selectLimiter() .
				'</td>
				</tr>
			</table>' ;
			
		 	
		return $listhtml . $this->getJsCode();
            
    }
	
	/**
	*
	**/
	function getFormHtml() {
		return
			"<div id=\"listform-div\" class=\"container-fluid\">" .
			    $this->getHtml()  .
			"</div>";	
	}
	
	/**
	* result limiter form
	**/
	function selectLimiter() {
		$opts = array(10,20,50,100);
		$limiter = $this->is_post() ? $_POST['limit'] : $_GET['limit'];
		$limiter = empty($limiter) ? $this->rows_per_page : $limiter;

		foreach($opts as $v) {
			$selected = $v == $limiter ? 'selected' :'';
			$options .= "<option value=\"{$v}\" {$selected}>{$v}</option>";
		}
		
		$change_evt = $this->is_ajax ? '' : 'onchange="changePageSize(this);"';
		$form = <<<EOD
			 <div>
				Rows Per Page <select name="limit" id="limit" {$change_evt}>
					{$options}
				</select>
			 </div>	
EOD;
		return $form;
	}
    
    /**
     *
     **/
    function paginate() {
        // call the parser
        $this->iterator->parseSQL();        
        //columns
        $columns = $this->iterator->columns_sql;        
        // join
        $join = $this->iterator->join_sql; 
        // where
        $where = $this->iterator->where_sql;
 
        // order
        $sort_c = $this->iterator->order_sql;
        
        $select_total = "SELECT COUNT(*) as total FROM {$this->iterator->table->_tablename}
                        {$join}
                        WHERE {$where}
                        ORDER BY {$sort_c}";
                                
        $result = $this->db->exec($select_total);
        if($result) {
        	$t_row = $result->fetch_object();
        }
        $this->total_rows = $t_row->total;

        //Max number of pages
		$limiter = $this->is_post() ? $_POST['limit'] : $_GET['limit'];
		$limiter = empty($limiter) ? $this->rows_per_page : $limiter;		
		$this->rows_per_page = $limiter;
        $this->max_pages = ceil($this->total_rows / $this->rows_per_page );
         if ($this->links_per_page > $this->max_pages) {
                $this->links_per_page = $this->max_pages;
         }         
    	      
        
        //Check the page value just in case someone is trying to input an aribitrary value
        if ($this->page > $this->max_pages || $this->page <= 0) {
                $this->page = 1;
        }
        
        //Calculate Offset
        $this->offset = $this->rows_per_page * ($this->page - 1);        
         
    }
    
    /**
     *
     **/
    function listHtml() {      
        // iterator setup
        $this->iterator->start         =   $this->offset; 
        $this->iterator->rows_per_page =   $this->rows_per_page;
 
        // listing html		        
        /*
        return    "\n\n" .  
        		"<form id=\"{$this->formName}-listing\">\n" .        			
        			$this->iterator->getHtml() . "\n" .
        		"</form>";
        */  
        
        // allow functions to modify the row data if callback is assigned
        $onRow = $this->onRow;
        $this->iterator->getEnum();
        if(!empty($onRow)) {          	      	
        	foreach($this->iterator->enum as $k=>$v) {
        		$row = $v;
        		if(!function_exists($onRow)) {
        			print_r('error : onRow callback function not defined.');
        			exit;
        		}
        		$onRow($row,$this);
        		$this->iterator->enum[$k]= $row;
        	}
        }               
         		
 		$style = $this->iterator->styleList();
        return
                "
                {$style}
				<form id=\"{$this->formName}-listing\">\n
						<table class=\"list\"   cellspacing=\"0\" cellpadding=\"1\">
						" .
						$this->iterator->getHeader() .
						$this->iterator->getRowsHtml() .
						$this->iterator->getFooter() .
						"
						</table>
				</form>
				";        
          
    }
    
    /**
     * 
     */
    function getButtonsHtml() {       	    
    	
    	 foreach($this->listButtons as $name=>$button) {
    	 	$attrs='';

    		foreach($button as $attrname=>$attr) {
    			if($attrname == 'value' || $attrname='onclick') {
    				continue;
    			}
    			$attrs .= "{$attrname}=\"{$attr}\" ";
     		}
    		
    		$onclick = $button['onclick'];

    	 	if(strlen($onclick)>0) {
    			$attrs .= " onclick=\"{$button['onclick']}\" ";	
    				 
    		}  else {
    	    	if($name =='add') {
    				$attrs .= " onclick=\"{$this->getAddUrl()}\" ";
    			}
    			if($name =='delete') {
    				$attrs .= " onclick=\"{$this->getDeleteUrl()}\" ";
    			}      			  
    		}    		
    		
    		$btn .= "<input type=\"button\" value=\"{$button['value']}\" {$attrs} /> ";
    		
    	} 
    	return $btn;
    }
    
    /**
     *
     **/
    function paginationHtml() {
    	$pagination  = $this->renderFullNav();	
    	 
        return 
            "<div id=\"pagination-links\">" .
                $pagination .
            "</div>";
    }
	
	/**
	*
	**/
	function paginationAttributes($page) {
		return " page='{$page}' sort='{$this->fields['sort']['value']}' order='{$this->fields['order']['value']}' rows_per_page='{$this->rows_per_page}'";
	}
    
    /**
     * Display the link to the first page
     *
     * @access public
     * @param string $tag Text string to be displayed as the link. Defaults to 'First'
     * @return string
     */	
    function renderFirst($tag = 'First', $prefix = '<span class="page_link">', $suffix = '</span>') {
            if ($this->total_rows == 0)
                    return FALSE;
            
            if ($this->page == 1) {
                    return "$tag ";
            } else {
                    return $prefix . '<a href="' . $this->php_self . '?page=1&' . $this->params . $this->sort_params. '"' . $this->paginationAttributes(1).'>' . $tag . '</a> ' .$suffix;
            }
    }
    
    /**
     * Display the link to the last page
     *
     * @access public
     * @param string $tag Text string to be displayed as the link. Defaults to 'Last'
     * @return string
     */
    function renderLast($tag = 'Last', $prefix = '<span class="page_link">', $suffix = '</span>') {
            if ($this->total_rows == 0)
                    return FALSE;
            
            if ($this->page == $this->max_pages) {
                    return $tag;
            } else {
                    return $prefix.  ' <a href="' . $this->php_self . '?page=' . $this->max_pages . '&' . $this->params . $this->sort_params . '"' . $this->paginationAttributes($this->max_pages).'>' . $tag . '</a>' .$suffix;
            }
    }
    
    /**
     * Display the next link
     *
     * @access public
     * @param string $tag Text string to be displayed as the link. Defaults to '>>'
     * @return string
     */
    function renderNext($tag = '&gt;&gt;', $prefix = '<span class="page_link">', $suffix = '</span>') {
            if ($this->total_rows == 0)
                    return FALSE;
            
            if ($this->page < $this->max_pages) {
                    return $prefix . '<a href="' . $this->php_self . '?page=' . ($this->page + 1) . '&' . $this->params . $this->sort_params . '"' . $this->paginationAttributes($this->page+1). '>' . $tag . '</a>' . $suffix;
            } else {
                    return $tag;
            }
    }
    
    /**
     * Display the previous link
     *
     * @access public
     * @param string $tag Text string to be displayed as the link. Defaults to '<<'
     * @return string
     */
    function renderPrev($tag = '&lt;&lt;' , $prefix = '<span class="page_link">', $suffix = '</span>') {
            if ($this->total_rows == 0)
                    return FALSE;
            
            if ($this->page > 1) {
                    return $prefix . ' <a href="' . $this->php_self . '?page=' . ($this->page - 1) . '&' . $this->params . $this->sort_params . '"' . $this->paginationAttributes($this->page -1). '>' . $tag . '</a>' . $suffix;
            } else {
                    return " $tag";
            }
    }
    
    /**
     * Display the page links
     *
     * @access public
     * @return string
     */
    function renderNav($prefix = '<span class="page_link">', $suffix = '</span>') {
            if ($this->total_rows == 0)
                    return FALSE;
            
            $batch = ceil($this->page / $this->links_per_page );
            $end = $batch * $this->links_per_page;
             
            if ($end > $this->max_pages) {
                    $end = $this->max_pages;
            }
            $start = $end - $this->links_per_page + 1;
            $links = '';
            
            
            for($i = $start; $i <= $end; $i ++) {
                    if ($i == $this->page) {
                            $links .= $prefix . " $i " . $suffix;
                    } else {
                            $links .= ' ' . $prefix . '<a href="' . $this->php_self . '?page=' . $i . '&' . $this->params . $this->sort_params. '"' . $this->paginationAttributes($i). '>' . $i . '</a>' . $suffix . ' ';
                    }
            }
            
            return $links;
    }
    
    /**
     * Display full pagination navigation
     *
     * @access public
     * @return string
     */
    function renderFullNav() {
            return $this->renderFirst() . '&nbsp;' . $this->renderPrev() . '&nbsp;' . $this->renderNav() . '&nbsp;' . $this->renderNext() . '&nbsp;' . $this->renderLast();
    }    
    
	/**
	* for Ajax Feature
	**/
	function getJsCode() {
		$js= <<<EOD
			<script>
				function changePageSize(obj) {
					document.{$this->getFormName()}.rows_per_page.value = obj.value
					document.{$this->getFormName()}.submit.click()
				}	
			</script>	
EOD;
        // selector javascript event        
        if($this->is_ajax) {
        	$event_delete = "javascript:refreshList();";		
        } else {
        	$event_delete = "document.{$this->formName}.submit.click()";
        }
        
        $selector_event_js = <<<EOD
        <script>
	        function toggleSelectors(checkbox) {	        
	        	var is_check = document.getElementById('{$this->formName}-listing').toggle_selectors.checked
	        	var ids = document.getElementById('{$this->formName}-listing').ids
	        	
	        	if(!ids.length) {
	        		ids.checked = is_check;
	        	}
	        	else {
		        	for(var i=0;i< ids.length; ++i ){
		        		ids[i].checked = is_check
		        	}	
				}
	        }
	        
			function {$this->formName}_delete() {
	        	var ids = document.getElementById('{$this->formName}-listing').ids
	        	 
	        	var _ids = [];
	        	if(!ids.length) {
	        		if(ids.checked == true) {
	        			_ids[0] = ids.value;
	        		}
	        	} else {	        	
		        	for(var i=0;i< ids.length; ++i ){	        		 
		        		if(ids[i].checked == true) {	        		         			 
		        			_ids[i] = ids[i].value
		        		}
		        	}			
	        	}	        	
	        	
	        	if(_ids.length > 0) {
	        		var ans = confirm('Delete Selected?')
	        		if(ans) {
	        			document.getElementById('{$this->formName}').selected_ids.value = _ids.join('|')
	        			document.getElementById('{$this->formName}').action_delete.value = 1
	        			document.getElementById('{$this->formName}').page.value = 1
	        			
        				{$event_delete}
	        			
	        		} 
	        	} else {
	        			alert('Select the items from the list.');
	        	}
	        	
	        }	        

        </script>
EOD;
	
		if(!$this->is_ajax) return $js . $selector_event_js;
		
		$jq_path = base_dir_uri();
		$js2 = $selector_event_js;
		$js2 .= <<<EOD
			<script src="{$jq_path}www/js/jquery/jquery-1.8.0.js"></script>
			<script>
				// same as $(document).ready(function{});
				(function($){
					//alert(1)
					//console.log($('table').length)
					
					// pagination
					$(".page_link a").live('click',function(e) {
						e.preventDefault()
						$('#{$this->getFormName()}').find('#page').val($(this).attr('page'))
						refreshList();						
					})
					//page size
					$('#limit').live('change',function(e) {
						e.preventDefault()
						$('#listform').find('#rows_per_page').val($(this).val())
						$('[id="limit"]').val($(this).val())
						refreshList(); 						
					});
					// sorting
					$(".sort-link").live('click',function(e) {
						e.preventDefault()
						$('#{$this->getFormName()}').find('#sort').val($(this).attr('sort'))
						$('#{$this->getFormName()}').find('#order').val($(this).attr('order'))
						refreshList();						
					})					
					
					$.refreshList = function() {
						$.ajax({
							url:"",
							type:"{$this->method}",
							data:$('#listform').serialize(),
							dataType:"json",
							beforeSend:function(e) {
							},
							success:function(response) {
								$('[id="pagination-links"]').html(response.pages)
								$('#listing-grid').html(response.list)
								
								if(response.success_message) {
									if($('#{$this->getFormName()}').find('.success-message')) {
										$('#{$this->getFormName()}').find('.success-message').html(response.success_message)
									}
								}
								
								if(response.failed_message) {
									if($('#{$this->getFormName()}').find('.failed-message')) {
										$('#{$this->getFormName()}').find('.failed-message').html(response.failed_message)
									}
								}								
							}
						
						})					
					}
				})(jQuery);
				
				function refreshList() {
					$.ajax({
							url:"",
							type:"{$this->method}",
							data:$('#listform').serialize(),
							dataType:"json",
							beforeSend:function(e) {
							},
							success:function(response) {
								$('[id="pagination-links"]').html(response.pages)
								$('#listing-grid').html(response.list)
								
								if(response.success_message) {
									if($('#{$this->getFormName()}').find('.success-message')) {
										$('#{$this->getFormName()}').find('.success-message').html(response.success_message)
									}
								}
								
								if(response.failed_message) {
									if($('#{$this->getFormName()}').find('.failed-message')) {
										$('#{$this->getFormName()}').find('.failed-message').html(response.failed_message)
									}
								}								
							}
						
						})
					}					
			</script>
EOD;
		return $js2;
	}
	
	/**
	* sets the list form whether in ajax mode or not
	**/
	function set_is_ajax($is_ajax) {
		$this->is_ajax = $is_ajax;
	}
	
	/**
	* checks if listform is submitted the traditional or ajax way
	**/
	function is_ajax_submit() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']);
	}	
	
    /**
     * 
     */
	function setAddUrl($url) {
		$this->add_url = $url;	
	}
	
	/**
	 * 
	 */
	function getAddUrl() {
		return $this->add_url;	
	}	
	
	/**
	 * 
	 */
	function setEditUrl($url) {
		$this->edit_url = $url;	
	}
	
	/**
	 * 
	 */
	function getEditUrl() {
		return $this->edit_url;	
	}
	
	/**
	 * 
	 */
	function setDeleteUrl($url) {
		$this->delete_url = $url;	
	}
	
	/**
	 * 
	 */
	function getDeleteUrl() {
		return $this->delete_url;	
	}
}

if(!function_exists('addSelector')) {
	function addSelector(&$row, &$iterator) {
		$id = $row[$iterator->table->_key];
		$checkbox = "<input type=\"checkbox\" value=\"{$id}\" class=\"selectors\" name=\"ids\"/>";		
		$row['row_selector'] = $checkbox;

	}	
}

?>